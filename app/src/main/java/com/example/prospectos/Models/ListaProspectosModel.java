package com.example.prospectos.Models;

import android.content.Context;
import android.util.Log;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ListaResponse;
import com.example.prospectos.Models.Interfaces.IListaProspectosModel;
import com.example.prospectos.Presenters.Interfaces.IListaProspectosPresenter;
import com.example.prospectos.Services.IProspectosCall;
import com.example.prospectos.Services.RetrofitInstance;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ListaProspectosModel implements IListaProspectosModel {

    IListaProspectosPresenter iListaProspectosPresenter;
    public ListaProspectosModel(IListaProspectosPresenter iListaProspectosPresenter){
        this.iListaProspectosPresenter = iListaProspectosPresenter;
    }

    @Override
    public void BuscarProspectos(Context context) {
        Gson gson = new Gson();



        IProspectosCall iProspectosCall = RetrofitInstance.getRetrofitInstance("http://192.168.68.106:3000/prospecto/").create(IProspectosCall.class);
        Call<ListaResponse> call = iProspectosCall.ListaProspectos();

        call.enqueue(new Callback<ListaResponse>() {
            @Override
            public void onResponse(Call<ListaResponse> call, Response<ListaResponse> response) {
                ListaResponse listaResponse = new ListaResponse();
                listaResponse = (ListaResponse) response.body();
                iListaProspectosPresenter.MostrarListaProspectos(listaResponse);
            }

            @Override
            public void onFailure(Call<ListaResponse> call, Throwable t) {
                Log.e(TAG, "onFailure ListaProspecto: "+ t.getMessage());
            }
        });

    }

    @Override
    public void MonstrarListaProspecto(AltaResponse response) {

    }
}

package com.example.prospectos.Models.Interfaces;

public interface IEvaluarProspectosModel {
    void EvaluarProspecto(String RFC, String status, String observaciones);
}

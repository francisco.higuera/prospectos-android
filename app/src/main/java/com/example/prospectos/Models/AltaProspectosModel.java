package com.example.prospectos.Models;

import android.util.Log;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.Models.Interfaces.IAltaProspectosModel;
import com.example.prospectos.Presenters.Interfaces.IAltaProspectosPresenter;
import com.example.prospectos.Services.IProspectosCall;
import com.example.prospectos.Services.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AltaProspectosModel implements IAltaProspectosModel {

    IAltaProspectosPresenter iAltaProspectosPresenter;
    public AltaProspectosModel(IAltaProspectosPresenter iAltaProspectosPresenter){
        this.iAltaProspectosPresenter = iAltaProspectosPresenter;

    }

    @Override
    public void AltaProspecto(ProspectoEntity prospecto) {

        IProspectosCall iProspectosCall = RetrofitInstance.getRetrofitInstance("http://192.168.68.106:3000/prospecto/").create(IProspectosCall.class);
        Call<AltaResponse> call = iProspectosCall.AltaProspectos(prospecto);

        call.enqueue(new Callback<AltaResponse>() {
            @Override
            public void onResponse(Call<AltaResponse> call, Response<AltaResponse> response) {
                AltaResponse altaResponse = new AltaResponse();
                altaResponse = (AltaResponse) response.body();

                iAltaProspectosPresenter.MostrarRespuestaAltaProspecto(altaResponse);
            }

            @Override
            public void onFailure(Call<AltaResponse> call, Throwable t) {
                Log.e(TAG, "onFailure AltaProspecto: "+ t.getMessage());
            }
        });
    }


}

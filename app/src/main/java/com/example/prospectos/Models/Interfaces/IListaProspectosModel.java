package com.example.prospectos.Models.Interfaces;

import android.content.Context;

import com.example.prospectos.Entities.AltaResponse;


public interface IListaProspectosModel {
    void BuscarProspectos( Context context);
    void MonstrarListaProspecto(AltaResponse response);
}

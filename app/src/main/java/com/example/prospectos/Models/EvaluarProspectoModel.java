package com.example.prospectos.Models;

import android.util.Log;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.EvaluarProspectoEntity;
import com.example.prospectos.Entities.EvaluarResponse;
import com.example.prospectos.Models.Interfaces.IEvaluarProspectosModel;
import com.example.prospectos.Presenters.Interfaces.IEvaluarProspectoPresenter;
import com.example.prospectos.Services.IProspectosCall;
import com.example.prospectos.Services.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class EvaluarProspectoModel implements IEvaluarProspectosModel {

    IEvaluarProspectoPresenter iEvaluarProspectoPresenter;

    public EvaluarProspectoModel(IEvaluarProspectoPresenter iEvaluarProspectoPresenter){
        this.iEvaluarProspectoPresenter = iEvaluarProspectoPresenter;
    }
    @Override
    public void EvaluarProspecto(String RFC, String status, String observaciones) {
        EvaluarProspectoEntity evaluarProspectoEntity = new EvaluarProspectoEntity(RFC,status,observaciones);
        EvaluarResponse evaluarResponse = new EvaluarResponse();

        IProspectosCall iProspectosCall = RetrofitInstance.getRetrofitInstance("http://192.168.68.106:3000/prospecto/").create(IProspectosCall.class);
        Call<EvaluarResponse> call = iProspectosCall.EvaluarProspectos(evaluarProspectoEntity);

        call.enqueue(new Callback<EvaluarResponse>() {
            @Override
            public void onResponse(Call<EvaluarResponse> call, Response<EvaluarResponse> response) {
                EvaluarResponse evaluarResponse = new EvaluarResponse();
                evaluarResponse = (EvaluarResponse) response.body();
                iEvaluarProspectoPresenter.ProcesarEvaluarProspecto(evaluarResponse);
            }

            @Override
            public void onFailure(Call<EvaluarResponse> call, Throwable t) {
                Log.e(TAG, "onFailure EvaluarProspecto: "+ t.getMessage());
            }
        });

        iEvaluarProspectoPresenter.ProcesarEvaluarProspecto(evaluarResponse);
    }
}

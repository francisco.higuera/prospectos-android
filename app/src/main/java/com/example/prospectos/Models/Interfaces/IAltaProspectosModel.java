package com.example.prospectos.Models.Interfaces;

import android.content.Context;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;

public interface IAltaProspectosModel {
    void AltaProspecto(ProspectoEntity prospecto);

}

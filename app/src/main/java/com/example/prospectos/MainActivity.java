package com.example.prospectos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.prospectos.Views.AltaProspectosView;
import com.example.prospectos.Views.ListaProspectosView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    AltaProspectosView altaProspectosView = new AltaProspectosView();
    ListaProspectosView listaProspectosView = new ListaProspectosView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationView);
        loadFragment(listaProspectosView);
    }
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationView = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.altaProspectos:
                    loadFragment(altaProspectosView);
                    return true;
                case R.id.listaProspectos:
                    loadFragment(listaProspectosView);
                    return true;
            }
            return false;
        }
    };

    public void loadFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container,fragment);
        transaction.commit();
    }
}
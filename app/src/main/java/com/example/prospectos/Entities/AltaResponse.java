package com.example.prospectos.Entities;

public class AltaResponse {
    private String message;
    private int type;
    private ProspectoEntity prospectoEntity;

    public AltaResponse(String message, int type, ProspectoEntity prospectoEntity) {
        this.message = message;
        this.type = type;
        this.prospectoEntity = prospectoEntity;
    }

    public AltaResponse(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ProspectoEntity getProspectoEntity() {
        return prospectoEntity;
    }

    public void setProspectoEntity(ProspectoEntity prospectoEntity) {
        this.prospectoEntity = prospectoEntity;
    }




}

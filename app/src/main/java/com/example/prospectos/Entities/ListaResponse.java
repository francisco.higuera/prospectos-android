package com.example.prospectos.Entities;

import java.util.ArrayList;

public class ListaResponse {
    private int type;
    private String message;
    private ArrayList<ProspectoEntity> data;

    public ListaResponse(){

    }

    public ListaResponse(int type, String message, ArrayList<ProspectoEntity> data) {
        this.type = type;
        this.message = message;
        this.data = data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ProspectoEntity> getData() {
        return data;
    }

    public void setData(ArrayList<ProspectoEntity> data) {
        this.data = data;
    }
}

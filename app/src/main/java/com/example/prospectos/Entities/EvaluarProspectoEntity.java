package com.example.prospectos.Entities;

public class EvaluarProspectoEntity {
    private String RFC;
    private String status;
    private String observaciones;


    public EvaluarProspectoEntity(String RFC, String status, String observaciones) {
        this.RFC = RFC;
        this.status = status;
        this.observaciones = observaciones;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}

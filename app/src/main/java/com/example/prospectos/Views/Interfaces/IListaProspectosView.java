package com.example.prospectos.Views.Interfaces;

import com.example.prospectos.Entities.ListaResponse;

public interface IListaProspectosView {
    void BuscarProspectos();
    void MostrarProspectos(ListaResponse response);
}

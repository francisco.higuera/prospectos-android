package com.example.prospectos.Views.Interfaces;

import com.example.prospectos.Entities.EvaluarResponse;

public interface IEvaluarProspectoView {
    void EvaluarProspecto(String RFC, String status, String observaciones);
    void MostrarRespuestaEvaluarProspecto(EvaluarResponse response);
}

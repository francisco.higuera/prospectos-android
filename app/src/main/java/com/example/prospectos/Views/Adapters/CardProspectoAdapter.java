package com.example.prospectos.Views.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.R;
import com.example.prospectos.Views.EvaluarProspectoView;

import java.util.ArrayList;

public class CardProspectoAdapter extends RecyclerView.Adapter<CardProspectoAdapter.ProspectoHolder> {

    Activity activity;
    ArrayList<ProspectoEntity> prospectoList;



    public CardProspectoAdapter(Activity activity, ArrayList<ProspectoEntity> prospectoList){
        this.activity = activity;
        this.prospectoList = prospectoList;
    }

    @NonNull
    @Override
    public CardProspectoAdapter.ProspectoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_prospecto,parent,false);
        return new ProspectoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProspectoHolder holder, int position) {
        holder.tvNombreCompleto.setText(prospectoList.get(position).getNombre()+" "+ prospectoList.get(position).getPrimerApellido() +" " + prospectoList.get(position).getSegundoApellido());
        holder.tvTextStatus.setText((prospectoList.get(position).getStatus()));

        if(holder.tvTextStatus.getText().toString().compareTo("Aprovado") == 0){
            holder.llStatus.setBackgroundColor(activity.getColor(R.color.greenAutorizadoList));
        }else{
            if(holder.tvTextStatus.getText().toString().compareTo("Rechazado") == 0){
                holder.llStatus.setBackgroundColor(activity.getColor(R.color.redRechazadoList));
            }else{
                holder.llStatus.setBackgroundColor(activity.getColor(R.color.bluePendienteList));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, EvaluarProspectoView.class);
                intent.putExtra("prospecto", prospectoList.get(position));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return prospectoList.size();
    }

    public class ProspectoHolder extends  RecyclerView.ViewHolder{
        private TextView tvNombreCompleto;
        private TextView tvTextStatus;
        private LinearLayout llStatus;
        public ProspectoHolder(@NonNull View itemView){
            super(itemView);
            tvNombreCompleto = (TextView) itemView.findViewById(R.id.nombreProspectoCompleto);
            tvTextStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            llStatus = (LinearLayout) itemView.findViewById(R.id.viewStatus);
        }
    }
}

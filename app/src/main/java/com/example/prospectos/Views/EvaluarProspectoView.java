package com.example.prospectos.Views;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.prospectos.Entities.EvaluarResponse;
import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.Presenters.EvaluarProspectoPresenter;
import com.example.prospectos.R;
import com.example.prospectos.Views.Helpers.NetworkConnection;
import com.example.prospectos.Views.Interfaces.IEvaluarProspectoView;

public class EvaluarProspectoView extends AppCompatActivity implements IEvaluarProspectoView {

    Button btnAprovarProspecto;
    Button btnRechazarProspecto;
    TextView tvNombre;
    TextView tvColonia;
    TextView tvCalle;
    TextView tvNumero;
    TextView tvCP;
    TextView tvTelefono;
    TextView tvRFC;
    EditText etObservaciones;
    LinearLayout llBotones;
    NetworkConnection networkConnection;
    EvaluarProspectoPresenter evaluarProspectoPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluar_prospecto);

        btnAprovarProspecto = findViewById(R.id.btnAceptarProspecto);
        btnRechazarProspecto = findViewById(R.id.btnRechazarProspecto);
        tvNombre = (TextView) findViewById(R.id.tvEvaluarNombre);
        tvColonia =(TextView) findViewById(R.id.tvEvaluarColonia);
        tvCalle = (TextView) findViewById(R.id.tvEvaluarCalle);
        tvNumero = (TextView) findViewById(R.id.tvEvaluarNumero);
        tvCP = (TextView) findViewById(R.id.tvEvaluarCP);
        tvTelefono = (TextView) findViewById(R.id.tvEvaluarTelefono);
        tvRFC = (TextView) findViewById(R.id.tvEvaluarRFC);
        etObservaciones = (EditText) findViewById(R.id.etEvaluarObservaciones);
        llBotones = (LinearLayout) findViewById(R.id.llBotones);

        Intent intent = getIntent();
        ProspectoEntity prospecto = (ProspectoEntity) intent.getSerializableExtra("prospecto");
        tvNombre.setText(prospecto.getNombre()+" "+ prospecto.getPrimerApellido()+ " " +prospecto.getSegundoApellido());
        tvColonia.setText(prospecto.getColonia());
        tvCalle.setText(prospecto.getCalle());
        tvNumero.setText(String.valueOf(prospecto.getNumero()));
        tvCP.setText(String.valueOf(prospecto.getCodigoPostal()));
        tvTelefono.setText(prospecto.getTelefono());
        tvRFC.setText(prospecto.getRFC());
        etObservaciones.setText(prospecto.getObservaciones());

        btnAprovarProspecto.setOnClickListener(btnAprovarProspectoClick);
        btnRechazarProspecto.setOnClickListener(btnRechazarProspectoClick);

        networkConnection = new NetworkConnection();
        evaluarProspectoPresenter = new EvaluarProspectoPresenter(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if(prospecto.getStatus().compareTo("Enviado") != 0){
            llBotones.setVisibility(View.GONE);
            etObservaciones.setEnabled(false);
        }
    }

    @Override
    public void EvaluarProspecto(String RFC, String status, String observaciones) {
        if(networkConnection.NetworkStatus(this)){
            evaluarProspectoPresenter.EvaluarProspecto(RFC,status,observaciones);
        }else{
            Toast.makeText(this, "Necesita estar conectado a internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void MostrarRespuestaEvaluarProspecto(EvaluarResponse response) {
        if(response.getType() == 0){
            Toast.makeText(this, "Se evaluo el prospecto", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Ha ocurrido un error, por favor intentelo mas tarde", Toast.LENGTH_SHORT).show();
        }

        finish();
    }


    View.OnClickListener btnAprovarProspectoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            EvaluarProspecto(tvRFC.getText().toString(), "Aprovado", etObservaciones.getText().toString());
        }
    };


    View.OnClickListener btnRechazarProspectoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (etObservaciones.getText().length() == 0){
                Toast.makeText(EvaluarProspectoView.this, "El campo observaciones es obligatorio", Toast.LENGTH_SHORT).show();
            }else{
                EvaluarProspecto(tvRFC.getText().toString(),"Rechazado",etObservaciones.getText().toString());
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
package com.example.prospectos.Views;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.Presenters.AltaProspectosPresenter;
import com.example.prospectos.R;
import com.example.prospectos.Views.Helpers.NetworkConnection;
import com.example.prospectos.Views.Interfaces.IAltaProspectosView;

public class AltaProspectosView extends Fragment implements IAltaProspectosView {


    EditText etNombre;
    EditText etPrimerApellido;
    EditText etSegundoApellido;
    EditText etCalle;
    EditText etNumero;
    EditText etColonia;
    EditText etCodigoPostal;
    EditText etTelefono;
    EditText etRFC;
    Button btnAltaPRospecto;

    AltaProspectosPresenter altaProspectosPresenter;
    NetworkConnection networkConnection = new NetworkConnection();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        altaProspectosPresenter = new AltaProspectosPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alta_prospectos, container, false);
        etNombre = view.findViewById(R.id.nombreProspecto);
        etPrimerApellido = view.findViewById(R.id.primerApellidoProspecto);
        etSegundoApellido = view.findViewById(R.id.segundoApellidoProspecto);
        etCalle = view.findViewById(R.id.calleProspecto);
        etNumero = view.findViewById(R.id.numeroProspecto);
        etColonia = view.findViewById(R.id.coloniaProspecto);
        etCodigoPostal = view.findViewById(R.id.codigoPostalProspecto);
        etTelefono = view.findViewById(R.id.telefonoProspecto);
        etRFC = view.findViewById(R.id.RFCProspecto);
        btnAltaPRospecto = view.findViewById(R.id.btnAltaProspecto);

        btnAltaPRospecto.setOnClickListener(btnAltaProspectoClick);

        return view;
    }

    @Override
    public void AltaProspecto(ProspectoEntity prospecto) {
        if(networkConnection.NetworkStatus(getContext())){
            altaProspectosPresenter.AltaProspecto(prospecto);
        }else{
            Toast.makeText(getContext(), "Necesita estar conectado a internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void MostrarRespuestaAltaProspecto(AltaResponse response) {
        Toast.makeText(getContext(), "Se ha dado de alta correctamente", Toast.LENGTH_SHORT).show();
        LimpiarTextos();
    }

    View.OnClickListener btnAltaProspectoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{
                if(ValidarTextos()){
                    String nombre = etNombre.getText().toString();
                    String primerApellido = etPrimerApellido.getText().toString();
                    String segundoApellido = etSegundoApellido.getText().toString();
                    String calle = etCalle.getText().toString();
                    int numero = Integer.parseInt(etNumero.getText().toString());
                    String colonia = etColonia.getText().toString();
                    int cp = Integer.parseInt(etCodigoPostal.getText().toString());
                    String telefono = etTelefono.getText().toString();
                    String RFC = etRFC.getText().toString();
                    ProspectoEntity prospecto = new ProspectoEntity(0,nombre,primerApellido,segundoApellido,calle,numero,colonia,cp,telefono,RFC,"Enviado","");
                    AltaProspecto(prospecto);
                }else{
                    Toast.makeText(getContext(), "Favor de llenar los campos", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception exception){
                Log.e("Alta Prospecto", "onClick: "+exception.getMessage() );
            }
        }
    };

    private boolean ValidarTextos(){
        boolean bandera = true;
        if(etNombre.getText().toString().trim().length() == 0){
            etNombre.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etNombre.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etPrimerApellido.getText().toString().trim().length() == 0){
            etPrimerApellido.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }
        else{
            etPrimerApellido.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etCalle.getText().toString().trim().length() == 0){
            etCalle.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etCalle.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etNumero.getText().toString().trim().length() == 0){
            etNumero.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etNumero.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etColonia.getText().toString().trim().length() == 0){
            etColonia.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etColonia.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etCodigoPostal.getText().toString().trim().length() == 0){
            etCodigoPostal.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etCodigoPostal.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etTelefono.getText().toString().trim().length() <= 7){
            etTelefono.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etTelefono.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        if(etRFC.getText().toString().trim().length() <= 12){
            etRFC.setBackground(getContext().getDrawable(R.drawable.custom_edittext_error));
            bandera = false;
        }else{
            etRFC.setBackground(getContext().getDrawable(R.drawable.custom_edittext));
        }
        return bandera;
    }

    private void LimpiarTextos(){
        etNombre.setText("");
        etPrimerApellido.setText("");
        etSegundoApellido.setText("");
        etCalle.setText("");
        etColonia.setText("");
        etCodigoPostal.setText("");
        etTelefono.setText("");
        etRFC.setText("");
        etNumero.setText("");
    }

}
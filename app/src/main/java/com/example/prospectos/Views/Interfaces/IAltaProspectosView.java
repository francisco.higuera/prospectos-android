package com.example.prospectos.Views.Interfaces;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;

public interface IAltaProspectosView {
    void AltaProspecto(ProspectoEntity prospecto);
    void MostrarRespuestaAltaProspecto(AltaResponse response);
}

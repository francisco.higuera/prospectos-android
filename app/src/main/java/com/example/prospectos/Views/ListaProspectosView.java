package com.example.prospectos.Views;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.prospectos.Entities.ListaResponse;
import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.Models.ListaProspectosModel;
import com.example.prospectos.Presenters.ListaProspectosPresenter;
import com.example.prospectos.R;
import com.example.prospectos.Views.Adapters.CardProspectoAdapter;
import com.example.prospectos.Views.Helpers.NetworkConnection;
import com.example.prospectos.Views.Interfaces.IListaProspectosView;

import java.util.ArrayList;


public class ListaProspectosView extends Fragment implements IListaProspectosView {
    RecyclerView recyclerView;
    ArrayList<ProspectoEntity> propectosList;
    ListaProspectosPresenter listaProspectosPresenter;
    NetworkConnection networkConnection;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        propectosList = new ArrayList<>();
        listaProspectosPresenter = new ListaProspectosPresenter(this);
        networkConnection = new NetworkConnection();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lista_prospectos, container, false);
        recyclerView = view.findViewById(R.id.rwProspectos);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(llm);
        BuscarProspectos();
        return view;
    }


    @Override
    public void BuscarProspectos() {
        if(networkConnection.NetworkStatus(getContext())){
            listaProspectosPresenter.BuscarProspectos(getContext());
        }else{
            Toast.makeText(getContext(), "Necesita estar conectado a internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void MostrarProspectos(ListaResponse response) {
        if(response.getData().size() >= 0){
            propectosList = response.getData();
            CardProspectoAdapter adapter = new CardProspectoAdapter(getActivity(), propectosList);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BuscarProspectos();
    }
}
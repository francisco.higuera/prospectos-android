package com.example.prospectos.Presenters.Interfaces;

import com.example.prospectos.Entities.EvaluarResponse;

public interface IEvaluarProspectoPresenter {

    void EvaluarProspecto(String RFC, String status, String observaciones);
    void ProcesarEvaluarProspecto(EvaluarResponse response);
}

package com.example.prospectos.Presenters;

import android.content.Context;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;
import com.example.prospectos.Models.AltaProspectosModel;
import com.example.prospectos.Presenters.Interfaces.IAltaProspectosPresenter;
import com.example.prospectos.Views.Interfaces.IAltaProspectosView;

public class AltaProspectosPresenter implements IAltaProspectosPresenter {

    IAltaProspectosView iAltaProspectosView;
    AltaProspectosModel altaProspectosModel;
    public AltaProspectosPresenter(IAltaProspectosView iAltaProspectosView){
        this.iAltaProspectosView = iAltaProspectosView;
        altaProspectosModel = new AltaProspectosModel(this);
    }
    @Override
    public void AltaProspecto(ProspectoEntity prospecto) {
        altaProspectosModel.AltaProspecto(prospecto);
    }

    @Override
    public void MostrarRespuestaAltaProspecto(AltaResponse response) {
        iAltaProspectosView.MostrarRespuestaAltaProspecto(response);
    }
}

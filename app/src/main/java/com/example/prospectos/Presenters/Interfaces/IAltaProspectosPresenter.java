package com.example.prospectos.Presenters.Interfaces;

import android.content.Context;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.ProspectoEntity;

public interface IAltaProspectosPresenter {
    void AltaProspecto(ProspectoEntity prospecto);
    void MostrarRespuestaAltaProspecto(AltaResponse response);
}

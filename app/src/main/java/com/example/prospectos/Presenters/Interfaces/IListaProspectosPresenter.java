package com.example.prospectos.Presenters.Interfaces;

import android.content.Context;

import com.example.prospectos.Entities.ListaResponse;

public interface IListaProspectosPresenter {
    void BuscarProspectos(Context context);
    void MostrarListaProspectos(ListaResponse response);
}

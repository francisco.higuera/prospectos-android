package com.example.prospectos.Presenters;

import android.content.Context;

import com.example.prospectos.Entities.ListaResponse;
import com.example.prospectos.Models.ListaProspectosModel;
import com.example.prospectos.Presenters.Interfaces.IListaProspectosPresenter;
import com.example.prospectos.Views.Interfaces.IListaProspectosView;

public class ListaProspectosPresenter implements IListaProspectosPresenter {

    IListaProspectosView iListaProspectosView;
    ListaProspectosModel listaProspectosModel;
    public ListaProspectosPresenter(IListaProspectosView listaProspectosView){
        this.iListaProspectosView = listaProspectosView;
        listaProspectosModel = new ListaProspectosModel(this);
    }

    @Override
    public void BuscarProspectos(Context context) {
        listaProspectosModel.BuscarProspectos(context);
    }

    @Override
    public void MostrarListaProspectos(ListaResponse response) {
        iListaProspectosView.MostrarProspectos(response);
    }
}

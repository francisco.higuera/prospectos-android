package com.example.prospectos.Presenters;

import com.example.prospectos.Entities.EvaluarResponse;
import com.example.prospectos.Models.EvaluarProspectoModel;
import com.example.prospectos.Presenters.Interfaces.IEvaluarProspectoPresenter;
import com.example.prospectos.Views.EvaluarProspectoView;
import com.example.prospectos.Views.Interfaces.IEvaluarProspectoView;

public class EvaluarProspectoPresenter implements IEvaluarProspectoPresenter {


    IEvaluarProspectoView iEvaluarProspectoView;
    EvaluarProspectoModel evaluarProspectoModel;

    public EvaluarProspectoPresenter(IEvaluarProspectoView iEvaluarProspectoView){
        this.iEvaluarProspectoView = iEvaluarProspectoView;
        evaluarProspectoModel = new EvaluarProspectoModel(this);
    }
    @Override
    public void EvaluarProspecto(String RFC, String status, String observaciones) {
        evaluarProspectoModel.EvaluarProspecto(RFC,status, observaciones);
    }

    @Override
    public void ProcesarEvaluarProspecto(EvaluarResponse response) {
        iEvaluarProspectoView.MostrarRespuestaEvaluarProspecto(response);
    }
}

package com.example.prospectos.Services;

import com.example.prospectos.Entities.AltaResponse;
import com.example.prospectos.Entities.EvaluarProspectoEntity;
import com.example.prospectos.Entities.EvaluarResponse;
import com.example.prospectos.Entities.ListaResponse;
import com.example.prospectos.Entities.ProspectoEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface IProspectosCall {

    @GET("getAll")
    Call<ListaResponse> ListaProspectos();

    @POST("CrearProspecto")
    Call<AltaResponse> AltaProspectos(@Body ProspectoEntity prospectoEntity);

    @PUT("ActualizarProspecto")
    Call<EvaluarResponse> EvaluarProspectos(@Body EvaluarProspectoEntity evaluarProspectoEntity);
}
